use crate::{Item, Prefix};
use terra_types::PositiveI16;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct ItemSlot {
	pub item: Item,
	pub prefix: Option<Prefix>,
	pub count: PositiveI16,
}

impl From<SingleItemSlot> for ItemSlot {
	fn from(value: SingleItemSlot) -> Self {
		ItemSlot {
			item: value.item,
			prefix: value.prefix,
			count: PositiveI16::new(1).expect("In bounds"),
		}
	}
}

#[cfg(feature = "convert")]
impl TryFrom<terra_items::ItemSlot> for ItemSlot {
	type Error = ();
	fn try_from(value: terra_items::ItemSlot) -> Result<Self, ()> {
		Ok(Self {
			item: value.item.try_into()?,
			prefix: value.prefix.and_then(|x| x.try_into().ok()),
			count: PositiveI16::new(value.count.get().min(i16::MAX as i32) as i16)
				.expect("Same min bound"),
		})
	}
}

#[cfg(feature = "convert")]
impl TryFrom<ItemSlot> for terra_items::ItemSlot {
	type Error = ();
	fn try_from(value: ItemSlot) -> Result<Self, ()> {
		Ok(Self {
			item: value.item.try_into()?,
			prefix: value.prefix.map(|x| x.into()),
			count: terra_types::PositiveI32::new(value.count.get() as i32).expect("Same min bound"),
		})
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct SingleItemSlot {
	pub item: Item,
	pub prefix: Option<Prefix>,
}

impl From<ItemSlot> for SingleItemSlot {
	fn from(item_slot: ItemSlot) -> Self {
		SingleItemSlot {
			item: item_slot.item,
			prefix: item_slot.prefix,
		}
	}
}

#[cfg(feature = "convert")]
impl TryFrom<terra_items::SingleItemSlot> for SingleItemSlot {
	type Error = ();
	fn try_from(value: terra_items::SingleItemSlot) -> Result<Self, ()> {
		Ok(Self {
			item: value.item.try_into()?,
			prefix: value.prefix.and_then(|x| x.try_into().ok()),
		})
	}
}

#[cfg(feature = "convert")]
impl TryFrom<SingleItemSlot> for terra_items::SingleItemSlot {
	type Error = ();
	fn try_from(value: SingleItemSlot) -> Result<Self, ()> {
		Ok(Self {
			item: value.item.try_into()?,
			prefix: value.prefix.map(|x| x.into()),
		})
	}
}
